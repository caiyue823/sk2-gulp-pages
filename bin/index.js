#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const inquirer = require('inquirer');
const ejs = require('ejs');

const {constants} = fs;
const {resolve} = path;
const templatePath = resolve(__dirname, '../template');
inquirer.prompt([{
    type: 'input',
    name: 'projectName',
    message: 'Please input your project(folder) name:'
}, {
    type: 'input',
    name: 'description',
    message: 'Please input your project description:'
}, {
    type: 'input',
    name: 'homepage',
    message: 'Please input your project homepage:'
}, {
    type: 'input',
    name: 'author',
    message: 'Please input your name:'
}, {
    type: 'input',
    name: 'authorUrl',
    message: 'Please input your url:'
}]).then(answers => {
    answers.projectName = answers.projectName || 'gulp-pages';
    const projectName = answers.projectName;
    // 获取命令行下要写入的文件夹路径
    const distPath = resolve(process.cwd(), projectName);
    fs.access(distPath, constants.F_OK, (err => {
        if (err) {
            // 未找到文件夹，直接安装
            install();
        } else {
            // 已有文件夹，询问是否继续
            inquirer.prompt([{
                message: `${projectName} folder already exists, do you want to remove it`,
                name: 'isRemove',
                type: 'confirm',
            }]).then(({isRemove}) => {
                if (isRemove) {
                    // 继续，删除已有文件夹并安装
                    fs.rmSync(distPath, {recursive: true, force: true});
                    console.log(`${projectName} existing folder removed`);
                    install();
                } else {
                    throw new Error('init error:aborted');
                }
            })
        }
    }))
    function install() {
        console.log('installation started');
        fs.mkdirSync(distPath);
        copy(templatePath, distPath, answers);
        console.log(`done, hope you enjoy!`);
    }
})

function copy(src, dist, templateData) {
    fs.readdir(src, (err, paths) => {
        if (err) {
            console.log('readdir error', src);
            return;
        }
        paths.forEach(path => {
            //src子文件/文件夹路径
            const srcPath = resolve(src, path);
            //dist子文件/文件夹路径
            const distPath = resolve(dist, path);
            fs.stat(srcPath, (err, stats) => {
                // 如果是文件夹
                if (stats.isDirectory()) {
                    // 先同步创建dist子文件夹
                    fs.mkdirSync(distPath);
                    // 递归执行方法
                    copy(srcPath, distPath, templateData);
                } else if (stats.isFile()) {
                    // 如果是文件
                    fs.readFile(srcPath, (err, data) => {
                        if (err) {
                            console.log('readFile error', srcPath);
                            return;
                        }
                        // 通过ejs编译
                        const template = ejs.compile(data.toString());
                        // 写入文件
                        fs.writeFile(distPath, template(templateData), 'utf-8', () => {
                            if (err) {
                                console.log('writeFile error', distPath);
                            }
                        })
                    })
                }
            })
        })
    })
}
