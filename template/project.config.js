// 示例的project.config.js
module.exports = {
  data: {
    menus: [
      {
        name: 'Home',
        icon: 'aperture',
        link: 'index.html'
      },
      {
        name: 'Features',
        link: 'features.html'
      },
      {
        name: 'About',
        link: 'about.html'
      },
      {
        name: 'Contact',
        link: '#',
        children: [
          {
            name: 'Twitter',
            link: 'https://twitter.com/w_zce'
          },
          {
            name: 'About',
            link: 'https://weibo.com/zceme'
          },
          {
            name: 'divider'
          },
          {
            name: 'About',
            link: 'https://github.com/zce'
          }
        ]
      }
    ],
    pkg: require('./package.json'),
    date: new Date(),
  },
  config: {
    SRC: 'src',
    DIST: 'dist',
    TEMP: '.tmp',
    PUBLIC: 'public',
    PATHS: {
      style: 'assets/styles/*.scss',
      script: 'assets/scripts/*.js',
      page: '*.html',
      image: 'assets/images/**',
      font: 'assets/fonts/**',
    }
  }
}
